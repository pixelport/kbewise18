package de.htw.ai.kbe.echo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SongJSONHelper implements SongHelper{

    @SuppressWarnings("unchecked")
    public synchronized List<Song> readToSongs(String filename) throws FileNotFoundException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        try (InputStream is = new BufferedInputStream(new FileInputStream(filename))) {
            return (List<Song>) objectMapper.readValue(is, new TypeReference<List<Song>>(){});
        }
    }

    // Write a List<Song> to a JSON-file
    public synchronized void writeToSongs(List<Song> songs, String filename) throws FileNotFoundException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        try (OutputStream os = new BufferedOutputStream(new FileOutputStream(filename))) {
            objectMapper.writeValue(os, songs);
        }
    }

    public synchronized Song readRequestPayloadToSong(String request_json) {
        ObjectMapper objectMapper = new ObjectMapper();
        Song result = null;
        try {
            result = objectMapper.readValue(request_json, Song.class);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }

    public synchronized String getSongListAsString(List<Song> songs) {
        String result = "";
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            result = objectMapper.writeValueAsString(songs);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public synchronized String getSong(List<Song> songs, Integer id) {
        ObjectMapper objectMapper = new ObjectMapper();
        for (Song s: songs) {
            if (s.getId().equals(id)) {
                try {
                    return objectMapper.writeValueAsString(s);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
