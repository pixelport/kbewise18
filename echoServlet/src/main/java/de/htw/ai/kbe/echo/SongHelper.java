package de.htw.ai.kbe.echo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface SongHelper {

    @SuppressWarnings("unchecked")
    List<Song> readToSongs(String filename) throws FileNotFoundException, IOException;
    void writeToSongs(List<Song> songs, String filename) throws FileNotFoundException, IOException;
    Song readRequestPayloadToSong(String request_json);
    String getSongListAsString(List<Song> songs);
    String getSong(List<Song> songs, Integer id);
}
