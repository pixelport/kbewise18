package de.htw.ai.kbe.echo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.OptionalInt;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

public class SongsRX extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private String uriToDB = null;

	public boolean isReadOnly = false;

	private static ArrayList<Song> songs;
	
	public ArrayList<Song> getSongs() {
	    return songs;
    }

    private String dbFilePath = null;

	@Override
	public void init(ServletConfig servletConfig) throws ServletException {
	    // Beispiel: Laden eines Konfigurationsparameters aus der web.xml
		this.uriToDB = servletConfig.getInitParameter("uriToDBComponent");
		this.dbFilePath = servletConfig.getInitParameter("dbFilePath");
        try {
            songs = (ArrayList<Song>) SongJSONHelper.readToSongs(this.dbFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy(){
	    // Wenn der Container entsorgt wird, schreibe songs in songs.json
        if (!isReadOnly) {
            try {
                SongJSONHelper.writeSongsToJSON(getSongs(), this.dbFilePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

	@Override
	public void doGet(HttpServletRequest request, 
	        HttpServletResponse response) throws IOException {
        response.setContentType("application/json");

		Enumeration<String> paramNames = request.getParameterNames();
		String responseStr = "";
		String accept_header = request.getHeader("Accept");
		if (accept_header != null) {
            if (!accept_header.trim().equals("*") && !accept_header.trim().equals("application/json")
                    && !accept_header.trim().equals("")) {
                    response.sendError(406);
                    return;
            }
        }
		String param = "";
		while (paramNames.hasMoreElements()) {
			param = paramNames.nextElement();
			if (param.equalsIgnoreCase("all")) {
			    responseStr += SongJSONHelper.getSongListAsString(songs) + "\n";
            }
            if (param.equalsIgnoreCase("songId")) {
                String[] paramValues = request.getParameterValues("songId");
                if (paramValues.length == 0){
                    response.sendError(400);
                    return;
                }
                Integer songId = null;
                try {
                    songId = Integer.parseInt(paramValues[0]);
                }
                catch (NumberFormatException ex){
                    response.sendError(400);
                    return;
                }
                String songStr = SongJSONHelper.getSong(songs, songId);
                if(songStr == null){
                    response.sendError(404);
                    return;
                }
                responseStr += songStr;
            }
		}

        try (PrintWriter out = response.getWriter()) {
            out.println(responseStr);
        }
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String requestStr = IOUtils.toString(request.getInputStream());
		Song new_song = SongJSONHelper.readJSONStringToSong(requestStr);
		if (new_song == null) {
		    response.sendError(400);
		    return;
        }
        Integer new_id = null;
        if (songs.size() == 0) {
            new_id = 1;
        }
        else {
            OptionalInt max_id = songs.stream().mapToInt(Song::getId).max();
            new_id = max_id.getAsInt() + 1;
        }
        new_song.setId(new_id);
        songs.add(new_song);

        response.setHeader("Location", "/songsServlet?songId=" + String.valueOf(new_id));
	}
	
	protected String getUriToDB () {
		return this.uriToDB;
	}
}