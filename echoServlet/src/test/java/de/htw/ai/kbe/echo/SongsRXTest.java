package de.htw.ai.kbe.echo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletConfig;

public class SongsRXTest {

    private SongsRX servlet;
    private MockServletConfig config;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private List<Song> songs;

    private final static String URITODB_STRING = "uriToDB:test.server";
    
    @Before
    public void setUp() throws ServletException {
        servlet = new SongsRX();
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        servlet.isReadOnly = true;
        config = new MockServletConfig();
        config.addInitParameter("uriToDBComponent", URITODB_STRING);
        config.addInitParameter("dbFilePath", "songs.json");
        servlet.init(config); //throws ServletException
        songs = servlet.getSongs();
    }

    /*
    @Test
    public void initShouldSetDBComponentURI() {
        assertEquals(URITODB_STRING, servlet.getUriToDB());
    }

    @Test
    public void doPostShouldAddSong() throws ServletException, IOException {
        String body = "{\"title\" : \"a new title\",\n" +
                "\"artist\" : \"YOU\",\n" +
                "\"album\" : \"Love you\",\n" +
                "\"released\" : 2017\n" +
                "}";
        assertTrue(songs.size() != 0);
        Integer expectedId = songs.stream().mapToInt(Song::getId).max().getAsInt() + 1;

        request.setContent(body.getBytes());
        servlet.doPost(request, response);
        assertEquals(response.getHeader("Location"), "/songsServlet?songId=" + expectedId);
        Song newSong = songs.stream().filter(s -> s.getId() == expectedId).findFirst().get();
        assertTrue(newSong != null);
        assertTrue(newSong.getTitle().equals("a new title"));
    }

    @Test
    public void doSongIdReturnsNotFound() throws IOException {
        request.addParameter("songId", "-1");
        servlet.doGet(request, response);

        assertTrue(response.getContentType().equals("application/json"));
        assertTrue(response.getStatus() == 404);
    }

    @Test
    public void doSongBadInput() throws IOException {
        request.addParameter("songId", "NoNumber");
        servlet.doGet(request, response);

        assertTrue(response.getContentType().equals("application/json"));
        assertTrue(response.getStatus() == 400);
    }




    @Test
    public void toSongsServletAllShouldReturnAll() throws ServletException, IOException {
        request.addParameter("all");
        servlet.doGet(request, response);

        assertTrue(response.getContentType().equals("application/json"));

        String respStr = response.getContentAsString();
        String expStr = SongJSONHelper.getSongListAsString(songs);
        assertEquals(respStr.equals(respStr), true);
    }
    */
}
