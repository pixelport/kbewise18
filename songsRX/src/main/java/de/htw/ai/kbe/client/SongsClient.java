package de.htw.ai.kbe.client;

import de.htw.ai.kbe.beans.Song;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class SongsClient {

    private static Client client = ClientBuilder.newClient();
    private static WebTarget baseTarget = client
            .target("http://localhost:8080/songsRX/rest/songs");


    private static String getToken(String username) {
        return client.target("http://localhost:8080/songsRX/rest/auth?userId=" + username).request(MediaType.TEXT_PLAIN).get(String.class);
    }

    private static void listSongs() {
        System.out.println("----- Listing all song entries ! -----");
        System.out.println("Generating TOKEN request ...");
        String token = getToken("eschuler");
        System.out.println(token);
        System.out.println("Generating request ...");
        String songs_json = baseTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, token).get(String.class);
        System.out.println("1. JSON String received : \n" + songs_json);
        System.out.println("Generating request ...");
        String songs_xml = baseTarget.request(MediaType.APPLICATION_XML).header(HttpHeaders.AUTHORIZATION, token).get(String.class);
        System.out.println("2. XML String received : \n" + songs_xml);
    }

    private static void listSongWithSpecifiedId(Integer id) {
        System.out.println("\n\n----- Listing song entry with id: " + id + "! -----");
        System.out.println("Generating request ...");
        System.out.println("Generating TOKEN request ...");
        String token = getToken("eschuler");
        System.out.println(token);
        String song_json = baseTarget.path(String.valueOf(id)).request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, token).get(String.class);
        System.out.println("1. JSON String received: \n" + song_json);
        System.out.println("Generating request ...");
        String song_xml = baseTarget.path(String.valueOf(id)).request(MediaType.APPLICATION_XML).header(HttpHeaders.AUTHORIZATION, token).get(String.class);
        System.out.println("2. XML String received: \n" + song_xml);
    }

    private static void writeSongAsJSON() {
        System.out.println("\n\n----- Writing a song entry to the database ! -----");
        Song s = new Song.Builder("Successful", "Drake")
                .album("So Far Gone")
                .released(new GregorianCalendar(2012, Calendar.FEBRUARY, 11).getTime())
                .build();
        Entity<Song> entity = Entity.json(s);
        System.out.println("Requesting following entry to be posted: \n" + s.toString());
        System.out.println("Generating TOKEN request ...");
        String token = getToken("eschuler");
        System.out.println(token);
        System.out.println("Generating request ...");
        Response response = baseTarget.request().accept(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, token).post(entity);
        System.out.println("1. Created following entry: \n"
                + "Response status: "+ response.getStatus() + " URI: " + response.getHeaderString("Location"));

    }

    private static void writeSongAsXML() {
        System.out.println("\n\n----- Writing a song entry to the database ! -----");
        Song s = new Song.Builder("Nope", "Someone")
                .album("REST Principles")
                .released(new GregorianCalendar(1999, Calendar.FEBRUARY, 11).getTime())
                .build();
        Entity<Song> entity = Entity.xml(s);
        System.out.println("Requesting following entry to be posted: \n" + s.toString());
        System.out.println("Generating TOKEN request ...");
        String token = getToken("eschuler");
        System.out.println(token);
        System.out.println("Generating request ...");
        Response response = baseTarget.request().header(HttpHeaders.AUTHORIZATION, token).post(entity);
        System.out.println("1. Created following entry: \n"
                + "Response status: "+ response.getStatus() + " URI: " + response.getHeaderString("Location"));
    }

    private static void patchSong() {
        System.out.println("\n\n----- Changing a song entry located in the database ! -----");
        Song s = new Song.Builder("Let it Change", "John Smith")
                .album("Making awesome puns since 1997")
                .released(new GregorianCalendar(1999, Calendar.FEBRUARY, 11).getTime())
                .build();
        Entity<Song> entity = Entity.xml(s);
        System.out.println("Requesting following entry to be changed: \n" + s.toString());
        System.out.println("Generating TOKEN request ...");
        String token = getToken("eschuler");
        System.out.println(token);
        System.out.println("Generating request ...");
        Response response = baseTarget.path(String.valueOf(12)).request().header(HttpHeaders.AUTHORIZATION, token).put(entity);
        System.out.println("1. Changed the entry, status code: " + response.getStatus());
    }

    private static void deleteSong() {
        System.out.println("\n\n----- Removing a song entry located in the database ! -----");
        System.out.println("Generating TOKEN request ...");
        String token = getToken("eschuler");
        System.out.println(token);
        System.out.println("Generating request ...");
        Response response = baseTarget.path(String.valueOf(12)).request().header(HttpHeaders.AUTHORIZATION, token).delete();
        System.out.println("1. Changed the entry, status code: " + response.getStatus());
    }
    public static void main(String[] args) {
        listSongs();
        listSongWithSpecifiedId(9);
        listSongWithSpecifiedId(9);
        listSongWithSpecifiedId(10);
        listSongWithSpecifiedId(3);
        writeSongAsJSON();
        writeSongAsXML();
        patchSong();
        deleteSong();
    }
}
