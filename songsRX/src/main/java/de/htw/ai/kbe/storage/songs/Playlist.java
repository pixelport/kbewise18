package de.htw.ai.kbe.storage.songs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.htw.ai.kbe.beans.Song;

import java.io.*;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Playlist implements InMemorySongDB{

    private static List<Song> songs;
    private static String jsonPath = "songs.json";

    public void setDbPath(String dbPath){
        jsonPath = dbPath;
    }

    public Playlist() {
        songs = new LinkedList<>();
        readSongValues(jsonPath);
    }

    public void readSongValues(String file) {
        try (InputStream is = new BufferedInputStream(new FileInputStream(file))) {
            songs = new ObjectMapper().readValue(is, new TypeReference<List<Song>>(){});
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private void utilsIDGenerator(Song s) {
        s.setId(Collections.max(songs, Comparator.comparingInt(Song::getId)).getId() + 1);
    }

    private String utilsSongToString(Song s) {
        try {
            return new ObjectMapper().writeValueAsString(s);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void writeToFile(String file) {
        ObjectMapper mapper = new ObjectMapper();
        try (OutputStream os = new BufferedOutputStream(new FileOutputStream(file))) {
            mapper.writeValue(os, songs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean POSTSong(Song s) {
        utilsIDGenerator(s);
        songs.add(s);
        String song = utilsSongToString(s);
        if (song == null) {
            return false;
        }
        writeToFile(jsonPath);
        return true;
    }

    public void PUTSong(Song payload, Song real_song) {
        payload.setId(real_song.getId());
        songs.set(songs.indexOf(real_song), payload);
        writeToFile(jsonPath);
    }

    public void DELETESong(Song payload) {
        songs.remove(payload);
        writeToFile(jsonPath);
    }

    public List<Song> GETSongs() {
        readSongValues(jsonPath);
        return songs;
    }

    public Song GETSong(Integer id) {
        if (songs.stream().anyMatch(song -> song.getId().equals(id))) {
            return songs.stream().filter(song -> song.getId().equals(id)).findFirst().get();
        }
        return null;
    }

}
