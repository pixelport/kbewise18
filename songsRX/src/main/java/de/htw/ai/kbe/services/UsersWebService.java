package de.htw.ai.kbe.services;

import de.htw.ai.kbe.storage.users.InMemoryUserDB;
import de.htw.ai.kbe.storage.users.UserGroup;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/auth")
public class UsersWebService {

    private InMemoryUserDB user_db;

    @Inject
    public UsersWebService(InMemoryUserDB user_db) {
        this.user_db = user_db;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response GETAuthenticityToken(@QueryParam("userId") String payload) {
        String token = user_db.generateToken(payload, 30);
        return (token == null) ? Response.status(Response.Status.FORBIDDEN).build() : Response.ok(token).build();
    }

}
