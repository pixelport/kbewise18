package de.htw.ai.kbe.test;

import de.htw.ai.kbe.services.SongsWebService;
import de.htw.ai.kbe.storage.songs.InMemorySongDB;
import de.htw.ai.kbe.storage.songs.Playlist;
import de.htw.ai.kbe.storage.users.InMemoryUserDB;
import de.htw.ai.kbe.storage.users.UserGroup;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.Assert;
import org.junit.Test;

import de.htw.ai.kbe.beans.Song;
import de.htw.ai.kbe.services.UsersWebService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebServiceTest extends JerseyTest {

    private final String testJsonFile = "test_songs.json";

    @Inject
    private InMemorySongDB song_db;

    @Inject
    private InMemoryUserDB user_db;

    @Override
    protected Application configure() {
        AbstractBinder binder = new AbstractBinder() {
            @Override
            protected void configure() {
                bind(Playlist.class).to(InMemorySongDB.class).in(Singleton.class);
                bind(UserGroup.class).to(InMemoryUserDB.class).in(Singleton.class);
            }
        };
        ResourceConfig config = new ResourceConfig(SongsWebService.class, UsersWebService.class).register(binder);
        final ServiceLocator locator = ServiceLocatorUtilities.bind(binder);
        locator.inject(this);
        return config;
    }

    @Test
    public void testDependencyInjectionInTestClass() {
        Assert.assertNotNull(song_db);
        Assert.assertNotNull(user_db);
    }

    private Response GenerateAuthorizationToken(String userId) {
        return target("/auth").queryParam("userId", userId)
                .request(MediaType.TEXT_PLAIN).buildGet().invoke();
    }

    @Test
    public void generateTokenValidUserTest() {
        user_db.addUser("mmuster");
        Response response = GenerateAuthorizationToken("mmuster");
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(response.readEntity(String.class).length(), 30);
    }

    @Test
    public void generateTokenInValidUserTest() {
        Assert.assertTrue(user_db.getUser("nonExistantUser") == null);
        Response response = GenerateAuthorizationToken("nonExistantUser");
        Assert.assertEquals(403, response.getStatus());
    }

    @Test
    public void putSongInvalidToken() {
        String token = "nonValidToken";

        Response responseXML = putSongTestHelper(token, MediaType.APPLICATION_XML);
        Response responseJSON = putSongTestHelper(token, MediaType.APPLICATION_JSON);
        Assert.assertEquals(401, responseXML.getStatus());
        Assert.assertEquals(401, responseJSON.getStatus());
    }

    private void prepareTestFile() throws FileNotFoundException {
        String testJson = "[{\"id\":7,\"title\":\"7 Years\",\"artist\":\"Lukas Graham\",\"album\":\"Lukas Graham (Blue Album)\",\"released\":2015}]";
        PrintWriter out = new PrintWriter(testJsonFile);
        out.write(testJson);
        out.close();
    }


    @Test
    public void putSongValidToken() throws IOException {
        String token = GenerateAuthorizationToken("mmuster").readEntity(String.class);

        String[] mediaTypes = new String[]{MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON};
        for (String mediaType : mediaTypes) {
            prepareTestFile();
            song_db.setDbPath(testJsonFile);
            song_db.readSongValues(testJsonFile);

            Response response = putSongTestHelper(token, mediaType);
            Assert.assertEquals(204, response.getStatus());

            List<String> lines = Files.readAllLines(Paths.get(testJsonFile), StandardCharsets.UTF_8);
            Assert.assertTrue(lines.size() == 1);
            String expectedJSON = "[{\"id\":7,\"title\":\"testTitle\",\"artist\":\"testArtist\",\"album\":\"testAlbum\",\"released\":1544990329696}]";
            Assert.assertEquals(lines.get(0), expectedJSON);
        }
    }



    private Response putSongTestHelper(String token, String mediaType){
        Song song = new Song();
        song.setAlbum("testAlbum");
        song.setTitle("testTitle");
        song.setArtist("testArtist");
        song.setReleased(new Date(1544990329696L));

        Entity<Song> songEntity = null;
        if(mediaType.equals(MediaType.APPLICATION_XML))
            songEntity = Entity.xml(song);
        else if(mediaType.equals(MediaType.APPLICATION_JSON))
            songEntity = Entity.json(song);

        return target("/songs/7")
                .request(mediaType).header(HttpHeaders.AUTHORIZATION, token).buildPut(songEntity).invoke();
    }
}
