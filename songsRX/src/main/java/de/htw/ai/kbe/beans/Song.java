package de.htw.ai.kbe.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.Year;
import java.util.Date;

@XmlRootElement(name = "song")
public class Song {

    private Integer id;
    private String title;
    private String artist;
    private String album;
    private Date released;

    public Song() {

    }

    public Song(Builder builder) {
        if (builder == null) {
            throw new NullPointerException("Builder object must be specified to create the song POJO ! ");
        }
        this.title = builder.title;
        this.artist = builder.artist;
        this.album = builder.album;
        this.released = builder.released;
    }

    public static class Builder {
        // Required params
        private Integer id;
        private String title;
        private String artist;
        // Optional params
        private String album;
        private Date released;

        public Builder(String title, String artist) {
            this.title = title;
            this.artist = artist;
        }

        public Builder album(String album) {
            this.album = album;
            return this;
        }

        public Builder released(Date released) {
            this.released = released;
            return this;
        }

        public Song build() { return new Song(this); }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public Date getReleased() {
        return released;
    }

    public void setReleased(Date released) {
        this.released = released;
    }

    @Override
    public String toString() {
        if (id == null) {
            return "SONG {ID NOT ASSIGNED YET}" + ":title=" + title + " artist=" + artist + " album=" + album + " released=" + released.toString();
        }
        return "SONG " + id + ":title=" + title + " artist=" + artist + " album=" + album + " released=" + released.toString();
    }
}
