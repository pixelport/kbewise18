package de.htw.ai.kbe.storage;

import de.htw.ai.kbe.storage.songs.InMemorySongDB;
import de.htw.ai.kbe.storage.songs.Playlist;
import de.htw.ai.kbe.storage.users.InMemoryUserDB;
import de.htw.ai.kbe.storage.users.UserGroup;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.inject.Singleton;


public class DependencyBinder extends AbstractBinder {
    @Override
    protected void configure() {
        bind(Playlist.class).to(InMemorySongDB.class);
        bind(UserGroup.class).to(InMemoryUserDB.class).in(Singleton.class);
    }
}
