package de.htw.ai.kbe.storage.users;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.htw.ai.kbe.beans.User;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class UserGroup implements InMemoryUserDB {
    private static HashMap<User, String> user_tokens;
    private static List<User> users;

    public UserGroup() {
        user_tokens = new HashMap<>();
        users = new LinkedList<>();
        readFromFile("users.json");
    }

    private static void readFromFile(String file) {
        try (InputStream is = new BufferedInputStream(new FileInputStream(file))) {
            users = new ObjectMapper().readValue(is, new TypeReference<List<User>>(){});
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public User getUser(String userId) {
        if (users.stream().anyMatch(user -> user.getUserId().equals(userId))) {
            return users.stream().filter(user -> user.getUserId().equals(userId)).findFirst().get();
        }
        return null;
    }

    public void addUser(String userId){
        User user = new User(new User.Builder(userId));
        users.add(user);
    }

    @Override
    public boolean checkAuthenticityToken(String authorization_header_content) {
        return user_tokens.containsValue(authorization_header_content);
    }

    @Override
    public String generateToken(String userId, int length) {
        User user = getUser(userId);
        String token = RandomStringUtils.randomAlphanumeric(length);
        if (user == null) {
            return null;
        }
        user_tokens.putIfAbsent(user, token);
        return user_tokens.get(user);
    }

}
