package de.htw.ai.kbe.storage.users;

import de.htw.ai.kbe.beans.User;

public interface InMemoryUserDB {

    /**
     * Checks if the authenticity header in the user request matches the token that the user has acquired
     * @param authenticity_header_content the authenticity header received from the user request
     * @return true if the authenticity token inside the header matches one in our user database
     */
    boolean checkAuthenticityToken(String authenticity_header_content);

    /**
     * Generates an authenticity token for the user with the specified userId
     * @param userId the userId
     * @param length how many chars the token has
     * @return the token
     */
    String generateToken(String userId, int length);

    void addUser(String userId);

    User getUser(String userId);
}
