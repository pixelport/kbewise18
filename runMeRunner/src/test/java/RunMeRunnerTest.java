import org.junit.Test;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.junit.Assert.*;

public class RunMeRunnerTest {

    @org.junit.Before
    public void setUp() throws Exception {

    }

    @Test
    public void withAllArgumentsTestIfReportExists() {
        String[] args = {"-c", "TestClass", "-o", "report.txt"};
        RunMeRunner.main(args);
        Path home_dir = Paths.get(System.getProperty("user.home"));
        Path real_path = Paths.get(home_dir.toString(), args[3]);
        File report_file = new File(real_path.toString());
        assertTrue(report_file.exists());
        assertTrue(report_file.canRead());
        try {
            String report = new String(Files.readAllBytes(Paths.get(report_file.getPath())));
            assertTrue(report.length() != 0);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testWithNoReport(){
        String[] args = {"-c", "TestClass"};

        // redirect stdout to stream for testing
        final ByteArrayOutputStream myOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myOut));

        RunMeRunner.main(args);

        final String standardOutput = myOut.toString();
        System.setOut(System.out);
        // check if testMethodWithoutRunMe
        assertTrue(standardOutput.contains("Methodennamen ohne @RunMe:\n" +
                "\ttestMethodWithoutRunMe"));

        // check if test methods without @RunMe exists
        assertTrue(standardOutput.contains("Methodennamen mit @RunMe:\n" +
                "\ttestMethodRunMeWithException\n" +
                "\ttestMethodRunMe")
                ||
                standardOutput.contains("Methodennamen mit @RunMe:\n" +
                        "\ttestMethodRunMe\n" +
                        "\ttestMethodRunMeWithException")
        );
    }

    @Test
    public void noClassGivenOnly_C_Option(){
        String[] args = {"-c"};

        // redirect stdout to stream for testing
        final ByteArrayOutputStream myOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myOut));

        RunMeRunner.main(args);

        final String standardOutput = myOut.toString();
        System.setOut(System.out);
        assertTrue(standardOutput.contains("Missing argument for option: c"));
        assertTrue(standardOutput.contains("usage: utility-argument"));
    }

    public void noArgsGivenShouldPrintHelp(){
        String[] args = {};

        // redirect stdout to stream for testing
        final ByteArrayOutputStream myOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myOut));

        RunMeRunner.main(args);

        final String standardOutput = myOut.toString();
        System.setOut(System.out);
        assertTrue(standardOutput.contains("Missing argument for option: c"));
        assertTrue(standardOutput.contains("usage: utility-argument"));
        assertTrue(standardOutput.contains(" -c <arg>   Input class:"));
        assertTrue(standardOutput.contains("-o <arg>   Report:"));
    }

    @Test
    public void testClassCouldNotBeFoundErrMsg(){
        String[] args = {"-c", "ClassWhichDoesNotExist"};

        // redirect stdout to stream for testing
        final ByteArrayOutputStream myOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myOut));

        RunMeRunner.main(args);

        final String standardOutput = myOut.toString();
        System.setOut(System.out);
        assertTrue(standardOutput.contains("Class could not be found, please enter a valid class name"));
    }

    private String getTestClassOutput(){
        String[] args = {"-c", "TestClass"};

        // redirect stdout to stream for testing
        final ByteArrayOutputStream myOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myOut));

        RunMeRunner.main(args);

        final String standardOutput = myOut.toString();
        System.setOut(System.out);
        return standardOutput;
    }

    @Test
    public void findAllMethodsWithoutRunMeAnnotation() throws ClassNotFoundException {
        String standardOutput = getTestClassOutput();

        assertTrue(standardOutput.contains("Methodennamen ohne @RunMe:\n" +
                "\t" + "testMethodWithoutRunMe"));
    }

    @Test
    public void findAllMethodsWithRunMeAnnotation() throws ClassNotFoundException {
        String standardOutput = getTestClassOutput();

        // check both possibilities
        assertTrue(standardOutput.contains("Methodennamen mit @RunMe:\n" +
                "\ttestMethodRunMe\n" +
                "\ttestMethodRunMeWithException") ||
                standardOutput.contains("Methodennamen mit @RunMe:\n" +
                        "\ttestMethodRunMeWithException\n" +
                        "\ttestMethodRunMe"
                ));
    }

    @Test
    public void findAllMethodsNonInvokable() throws ClassNotFoundException {
        String standardOutput = getTestClassOutput();

        assertTrue(standardOutput.contains("Nicht-invokierbare Methoden mit @RunMe: \n" +
                "\ttestMethodRunMeWithException: java.lang.reflect.InvocationTargetException"));
    }
}
