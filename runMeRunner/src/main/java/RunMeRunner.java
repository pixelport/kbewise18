import org.apache.commons.cli.*;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.*;
public class RunMeRunner {


    public static void main(String [] args) {
        Options options = new Options();

        // Creating the class name option
        Option class_name_option = new Option("c", true, "Input class: ");
        class_name_option.setArgs(1);
        class_name_option.setRequired(true);
        class_name_option.setType(String.class);
        options.addOption(class_name_option);

        // Creating the report option
        Option report = new Option("o", true, "Report: ");
        report.setArgs(1);
        report.setType(String.class);
        options.addOption(report);

        // Creating the necessary objects to parse and format options/usage information etc.
        CommandLineParser cmd_parser = new DefaultParser();
        HelpFormatter help_formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = cmd_parser.parse(options, args);
            String report_txt = null;
            if (cmd.getOptionValue('c') != null) {
                Class cl = Class.forName(cmd.getOptionValue('c'));
                System.out.println(class_name_option.getDescription() + cl.getName());
                report_txt = generateRunMeReport(cl);
            }
            if (cmd.getOptionValue('o') != null) {
                System.out.println(report.getDescription() + cmd.getOptionValue('o'));
                Path home_dir = Paths.get(System.getProperty("user.home"));
                Path file_path = Paths.get(home_dir.toString(), cmd.getOptionValue('o'));
                writeReport(file_path.toString(), report_txt);
            }
            else {
                System.out.println(report_txt);
            }
        }
        catch (ParseException e) {
            System.out.println(e.getMessage());
            help_formatter.printHelp("utility-argument", options);
        }
        catch (ClassNotFoundException e) {
            System.out.println("Class could not be found, please enter a valid class name");
            help_formatter.printHelp("utility-argument", options);
        }
    }

    private static String generateRunMeReport(Class c) {
        StringBuilder reportRes = new StringBuilder();

        List<Method> runMeMethods = new LinkedList<Method>();
        List<Method> nonRunMeMehods = new LinkedList<Method>();

        for (Method method: c.getDeclaredMethods()) {
            Annotation[] methodAnnotations = method.getDeclaredAnnotations();
            boolean foundRunMeAnnotation = false;
            for(Annotation annotation: methodAnnotations){
                if(annotation.annotationType().equals(RunMe.class)){
                    foundRunMeAnnotation = true;
                    break;
                }
            }

            if(foundRunMeAnnotation){
                runMeMethods.add(method);
            }
            else{
                nonRunMeMehods.add(method);
            }
        }

        reportRes.append("Methodennamen ohne @RunMe:\n");
        for (Method nonRunMeMethod: nonRunMeMehods) {
            reportRes.append("\t" + nonRunMeMethod.getName() + "\n");
        }

        reportRes.append("Methodennamen mit @RunMe:\n");
        for (Method runMeMethod: runMeMethods) {
            reportRes.append("\t" + runMeMethod.getName() + "\n");
        }

        reportRes.append("Nicht-invokierbare Methoden mit @RunMe: \n");
        for (Method runMeMethod: runMeMethods) {
            try {
                runMeMethod.invoke(c);
            } catch (Exception e) {
                reportRes.append("\t" + runMeMethod.getName() + ": " + e.getClass().getName() +"\n");
            }
        }
        return reportRes.toString();
    }

    private static void writeReport(String path, String report) {
        try {
            Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "utf-8"));
            writer.write(report);
            writer.close();
        } catch (UnsupportedEncodingException e) {
            System.out.println("The encoding you have provided is not supported");
        } catch (FileNotFoundException e) {
            System.out.println("File was not created/found");
        } catch (IOException e) {
            System.out.println("For some reason we couldn't write to the file");
        }
    }


}
