package de.htw.ai.kbe;

import de.htw.ai.kbe.beans.Song;
import de.htw.ai.kbe.beans.Songlist;
import de.htw.ai.kbe.beans.User;
import de.htw.ai.kbe.storage.songlists.UserSonglists;
import de.htw.ai.kbe.storage.users.UserGroup;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DBConnector {
    private static final String PERSISTENCE_UNIT_NAME = "songDB-PU";

    public static void main(String[] args) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager manager = factory.createEntityManager();
        List<Songlist> query = manager.createQuery("SELECT s FROM Songlist s", Songlist.class).getResultList();
        Songlist to_delete = query.get(query.size() - 1);
        manager.getTransaction().begin();
        manager.remove(to_delete);
        manager.getTransaction().commit();
        manager.close();
        factory.close();
    }
}
