package de.htw.ai.kbe.storage.songlists;

import de.htw.ai.kbe.beans.Song;
import de.htw.ai.kbe.beans.Songlist;
import de.htw.ai.kbe.beans.User;

import java.util.List;

public interface RelationalSonglistDB {
    /**
     * Returns all the songlists
     * @param username the user whose songlists need to be returned
     * @param all_playlists specifies if all the playlists for the given user need to be returned, if false, only
     *                      the public ones get returned, else all of them
     * @return all the songlists
     */
    List<Songlist> GETSonglists(String username, boolean all_playlists);

    /**
     * Returns the songlist with the specified id
     * @param id the id
     * @return the songlist with the id
     */
    Songlist GETSonglist(Integer id);

    /**
     * Posts the songlist into the DB
     * @param s the songlist to be posted
     * @return true, if POST was successful
     */
    boolean POSTSonglist(Songlist s);


    /**
     * Deletes a song entry from the DB
     * @param s the song to be deleted
     * @return true if song was successfully deleted from the database
     */
    boolean DELETESonglist(Songlist s);
}
