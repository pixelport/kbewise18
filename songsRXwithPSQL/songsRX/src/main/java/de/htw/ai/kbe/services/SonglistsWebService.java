package de.htw.ai.kbe.services;

import de.htw.ai.kbe.beans.Song;
import de.htw.ai.kbe.beans.Songlist;
import de.htw.ai.kbe.beans.User;
import de.htw.ai.kbe.storage.songlists.RelationalSonglistDB;
import de.htw.ai.kbe.storage.songs.InMemorySongDB;
import de.htw.ai.kbe.storage.users.InMemoryUserDB;
import org.apache.commons.collections.CollectionUtils;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import java.util.Set;

@Path("/songLists")
public class SonglistsWebService {

    private InMemorySongDB song_db;
    private InMemoryUserDB user_db;
    private RelationalSonglistDB songlist_db;

    @Inject
    public SonglistsWebService(InMemorySongDB song_db, InMemoryUserDB user_db, RelationalSonglistDB songlist_db) {
        this.song_db = song_db;
        this.user_db = user_db;
        this.songlist_db = songlist_db;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response GETSonglists(@QueryParam("userId") String username, @HeaderParam("Authorization") String authorization) {
        if (user_db.checkAuthenticityToken(authorization)) {
            /*
             * If the authorization header contains mmuster's authenticity token, but the user requests eschuler's songlist
             * then he should only get eschuler's public songlists, and via all_playlists the check is made
             */
            boolean all_playlists = user_db.getToken(user_db.getUser(username)).equals(authorization);
            List<Songlist> songlists = songlist_db.GETSonglists(username, all_playlists);
            return (songlists == null) ? Response.status(Response.Status.BAD_REQUEST).build()
                    : Response.ok(new GenericEntity<List<Songlist>>(songlists){}).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response GETSonglist(@PathParam("id") Integer id, @HeaderParam("Authorization") String authorization) {
        if (user_db.checkAuthenticityToken(authorization)) {
            Songlist songlist = songlist_db.GETSonglist(id);
            if (songlist == null) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            User songlist_holder = user_db.getUser(songlist.getOwner().getUserId());
            String token = user_db.getToken(songlist_holder);
            if (!songlist.getIsPublic()) {
                return (token.equals(authorization)) ? Response.ok(songlist).build()
                        : Response.status(Response.Status.FORBIDDEN).build();
            }
            return Response.ok(songlist).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response POSTSonglist(Songlist payload, @Context UriInfo info, @HeaderParam("Authorization") String authorization) {
        if (user_db.checkAuthenticityToken(authorization)) {
            User owner = user_db.getUserViaToken(authorization);
            payload.setOwner(owner);
            List<Song> songs = song_db.GETSongs();
            if (!songs.containsAll(payload.getSongs())) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            if (songlist_db.POSTSonglist(payload)) {
                UriBuilder uri_builder = info.getAbsolutePathBuilder();
                uri_builder.path(Integer.toString(payload.getList_id()));
                return Response.created(uri_builder.build()).build();
            }
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }
    @DELETE
    @Path("{id}")
    public Response DELETESonglist(@PathParam("id") Integer id, @HeaderParam("Authorization") String authorization) {
        if (user_db.checkAuthenticityToken(authorization)) {
            Songlist songlist = songlist_db.GETSonglist(id);
            if (songlist == null) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            User owner = user_db.getUser(songlist.getOwner().getUserId());
            String token = user_db.getToken(owner);
            if (token.equals(authorization)) {
                return (songlist_db.DELETESonglist(songlist)) ? Response.status(Response.Status.NO_CONTENT).build()
                        : Response.status(Response.Status.BAD_REQUEST).build();
            }
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }
}
