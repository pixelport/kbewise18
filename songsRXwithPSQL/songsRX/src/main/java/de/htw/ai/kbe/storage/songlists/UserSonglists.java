package de.htw.ai.kbe.storage.songlists;

import de.htw.ai.kbe.beans.Song;
import de.htw.ai.kbe.beans.Songlist;
import de.htw.ai.kbe.beans.User;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UserSonglists implements RelationalSonglistDB {

    private EntityManagerFactory entityManagerFactory;

    @Inject
    public UserSonglists(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public List<Songlist> GETSonglists(String username, boolean all_playlists) {
        EntityManager manager = entityManagerFactory.createEntityManager();
        try {
            TypedQuery<Songlist> query = manager.createQuery("SELECT s FROM Songlist s", Songlist.class);
            List<Songlist> songlists = query.getResultList().stream().
                    filter(songlist -> songlist.getOwner().getUserId().equals(username)).collect(Collectors.toList());
            return (all_playlists) ? songlists :
                    songlists.stream().filter(Songlist::getIsPublic).collect(Collectors.toList());
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Could not query the songlists");
            return null;
        }
        finally {
            manager.close();
        }
    }

    @Override
    public Songlist GETSonglist(Integer id) {
        EntityManager manager = entityManagerFactory.createEntityManager();
        try {
            Songlist s = manager.find(Songlist.class, id);
            return s;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Could not query the songlist with the specified id");
            return null;
        }
        finally {
            manager.close();
        }
    }

    @Override
    public boolean POSTSonglist(Songlist s) {
        EntityManager manager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = manager.getTransaction();
        try {
            transaction.begin();
            manager.persist(s);
            transaction.commit();
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Could not save the songlist to the database");
            transaction.rollback();
            return false;
        }
        finally {
            manager.close();
        }
    }

    @Override
    public boolean DELETESonglist(Songlist s) {
        EntityManager manager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = manager.getTransaction();
        try {
            Songlist songlist = manager.find(Songlist.class, s.getList_id());
            transaction.begin();
            manager.remove(songlist);
            transaction.commit();
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Could not delete songlist from the database");
            transaction.rollback();
            return false;
        }
        finally {
            manager.close();
        }
    }
}
