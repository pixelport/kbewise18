package de.htw.ai.kbe.storage.songs;

import de.htw.ai.kbe.beans.Song;

import java.util.List;

public interface InMemorySongDB {

    /**
     * Returns all the songs
     * @return all the songs
     */
    List<Song> GETSongs();

    /**
     * Returns the song with the specified id
     * @param id the id
     * @return the song with the id
     */
    Song GETSong(Integer id);

    /**
     * Posts the song into the DB
     * @param s the song to be posted
     * @return true, if POST was successful
     */
    boolean POSTSong(Song s);

    /**
     * Patches real_song to match the payload
     * @param payload the payload
     * @param real_song the song in the DB to be changed
     */
    void PUTSong(Song payload, Song real_song);

    /**
     * Deletes a song entry from the DB
     * @param id the id of the song to be deleted
     * @return true if song was successfully deleted from the database
     */
    boolean DELETESong(Integer id);
}
