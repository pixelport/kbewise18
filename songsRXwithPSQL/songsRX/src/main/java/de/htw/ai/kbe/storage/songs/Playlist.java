package de.htw.ai.kbe.storage.songs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.htw.ai.kbe.beans.Song;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.*;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;


public class Playlist implements InMemorySongDB {

    private EntityManagerFactory entityManagerFactory;

    @Inject
    public Playlist(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public boolean POSTSong(Song s) {
        EntityManager manager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = manager.getTransaction();
        try {
            transaction.begin();
            manager.persist(s);
            transaction.commit();
            return true;
        }
        catch (Exception e) {
            System.out.println("Could not save song to the database");
            transaction.rollback();
            return false;
        }
        finally {
            manager.close();
        }
    }

    public void PUTSong(Song payload, Song real_song) {
        EntityManager manager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = manager.getTransaction();
        try {
            transaction.begin();
            payload.setId(real_song.getId());
            manager.merge(payload);
            transaction.commit();
        }
        catch (Exception e) {
            transaction.rollback();
            throw new PersistenceException("Could not persist entity: " + e.toString());
        }
        finally {
            manager.close();
        }
    }

    public boolean DELETESong(Integer id) {
        EntityManager manager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = manager.getTransaction();
        try {
            Song song = manager.find(Song.class, id);
            transaction.begin();
            manager.remove(song);
            transaction.commit();
            return true;
        }
        catch (Exception e){
            System.out.println("Could not remove song");
            transaction.rollback();
            return false;
        }
        finally {
            manager.close();
        }
    }

    public List<Song> GETSongs() {
        EntityManager manager = entityManagerFactory.createEntityManager();
        try {
            TypedQuery<Song> query = manager.createQuery("SELECT s FROM Song s", Song.class);
            return query.getResultList();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Could not retrieve songs from database");
            return null;
        }
        finally {
            manager.close();
        }
    }

    public Song GETSong(Integer id) {
        EntityManager manager = entityManagerFactory.createEntityManager();
        Song s;
        try {
            s = manager.find(Song.class, id);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Could not find song with specified id");
            s = null;
        }
        finally {
            manager.close();
        }
        return s;
    }

}
