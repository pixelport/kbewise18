package de.htw.ai.kbe.beans;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "enduser")
@XmlRootElement(name = "user")
public class User  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String userId;
    private String lastName;
    private String firstName;
    /*
    @OneToMany(mappedBy = "owner",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY)
    private Set<Songlist> songlists;*/

    public User() {

    }

    public User(Builder builder) {
        if (builder == null) {
            throw new NullPointerException("Builder object must be specified to create the user POJO ! ");
        }
        this.userId = builder.userId;
        this.lastName = builder.lastName;
        this.firstName = builder.firstName;
    }

    public static class Builder {
        private Integer id;
        private String userId;
        private String lastName;
        private String firstName;

        public Builder(String userId) {
            this.userId = userId;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public User build() { return new User(this); }
    }
    /*
    public Set<Songlist> getSonglists() {
        if (songlists == null) {
            songlists = new HashSet<>();
        }
        return songlists;
    }

    public void setSonglists(Set<Songlist> songlists) {
        if (this.songlists == null) {
            this.songlists = new HashSet<>();
        }
        this.songlists = songlists;
        if (songlists != null) {
            this.songlists.forEach(songlist -> songlist.setOwner(this));
        }
    }

    public void addSonglist(Songlist songlist) {
        if (songlists == null) {
            songlists = new HashSet<>();
        }
        songlist.setOwner(this);
        songlists.add(songlist);
    }
    */
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String toString() {
        return "userId= " + userId + " lastName= " + lastName + " firstName=" + firstName;
    }
}
