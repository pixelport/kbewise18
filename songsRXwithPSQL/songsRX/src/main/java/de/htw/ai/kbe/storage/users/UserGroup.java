package de.htw.ai.kbe.storage.users;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.htw.ai.kbe.beans.User;
import org.apache.commons.lang3.RandomStringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Singleton
public class UserGroup implements InMemoryUserDB {
    private static HashMap<User, String> user_tokens;
    private static List<User> users;

    private EntityManagerFactory entityManagerFactory;

    @Inject
    public UserGroup(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
        user_tokens = new HashMap<>();
        users = loadUsers();
    }

    public List<User> loadUsers() {
        List<User> loadedUsers = new LinkedList<>();
        EntityManager manager = entityManagerFactory.createEntityManager();
        try {
            TypedQuery<User> query = manager.createQuery("SELECT u from User u", User.class);
            loadedUsers = query.getResultList();
        }
        finally {
            manager.close();
        }
        return loadedUsers;
    }


    public User getUser(String userId) {
        if (users.stream().anyMatch(user -> user.getUserId().equals(userId))) {
            return users.stream().filter(user -> user.getUserId().equals(userId)).findFirst().get();
        }
        return null;
    }

    @Override
    public User getUserViaToken(String token) {
        for (Map.Entry<User, String> entry: user_tokens.entrySet()) {
            if (token.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    @Override
    public String getToken(User user) {
        return user_tokens.get(user);
    }

    @Override
    public boolean checkAuthenticityToken(String authorization_header_content) {
        return user_tokens.containsValue(authorization_header_content);
    }

    @Override
    public String generateToken(String userId, int length) {
        User user = getUser(userId);
        String token = RandomStringUtils.randomAlphanumeric(length);
        if (user == null) {
            return null;
        }
        user_tokens.putIfAbsent(user, token);
        return user_tokens.get(user);
    }

}
