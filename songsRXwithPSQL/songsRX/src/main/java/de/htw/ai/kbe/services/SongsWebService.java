package de.htw.ai.kbe.services;

import de.htw.ai.kbe.beans.Song;
import de.htw.ai.kbe.storage.songs.InMemorySongDB;
import de.htw.ai.kbe.storage.users.InMemoryUserDB;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;


@Path("/songs")
public class SongsWebService {

    private InMemorySongDB song_db;
    private InMemoryUserDB user_db;

    @Inject
    public SongsWebService(InMemorySongDB song_db, InMemoryUserDB user_db) {
        this.song_db = song_db;
        this.user_db = user_db;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response GETSongs(@HeaderParam("Authorization") String token) {
        if (user_db.checkAuthenticityToken(token)) {
            List<Song> songs = song_db.GETSongs();
            // Generic Entity is required, because the class List doesn't have the XmlRootElement annotation
            return (songs == null) ? Response.status(404).build() : Response.ok(new GenericEntity<List<Song>>(songs) {}).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{id}")
    public Response GETSong(@PathParam("id") Integer id, @HeaderParam("Authorization") String token) {
        if (user_db.checkAuthenticityToken(token)) {
            Song s = song_db.GETSong(id);
            return (s == null) ? Response.status(Response.Status.BAD_REQUEST).build() : Response.ok(s).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response POSTSong(Song s, @Context UriInfo info, @HeaderParam("Authorization") String token) {
        if (user_db.checkAuthenticityToken(token)) {
            if (song_db.POSTSong(s)) {
                UriBuilder uri_builder = info.getAbsolutePathBuilder();
                uri_builder.path(Integer.toString(s.getId()));
                return Response.created(uri_builder.build()).build();
            }
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{id}")
    public Response PUTSong(Song payload, @PathParam("id") Integer id, @HeaderParam("Authorization") String token) {
        if (user_db.checkAuthenticityToken(token)) {
            Song real_song = song_db.GETSong(id);
            song_db.PUTSong(payload, real_song);
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @DELETE
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{id}")
    public Response DELETESong(@PathParam("id") Integer id, @HeaderParam("Authorization") String token) {
        if (user_db.checkAuthenticityToken(token)) {
            return (song_db.DELETESong(id)) ? Response.status(Response.Status.NO_CONTENT).build()
                    : Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

}
