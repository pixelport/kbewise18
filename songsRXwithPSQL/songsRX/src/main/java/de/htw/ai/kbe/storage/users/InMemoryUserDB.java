package de.htw.ai.kbe.storage.users;

import de.htw.ai.kbe.beans.User;

public interface InMemoryUserDB {

    /**
     * Checks if the authenticity header in the user request matches the token that the user has acquired
     * @param authenticity_header_content the authenticity header received from the user request
     * @return true if the authenticity token inside the header matches one in our user database
     */
    boolean checkAuthenticityToken(String authenticity_header_content);

    /**
     * Generates an authenticity token for the user with the specified userId
     * @param userId the userId
     * @param length how many chars the token has
     * @return the token
     */
    String generateToken(String userId, int length);

    /**
     * Returns the user with the specified userid
     * @param userId the userid to query the user with
     * @return the user
     */
    User getUser(String userId);

    /**
     * Returns the user with the specified token
     * @param token the token
     * @return the user
     */
    User getUserViaToken(String token);

    /**
     * Returns the token of the user specified as a paremeter
     * @param user this users token needs to be returned
     * @return the token
     */
    String getToken(User user);
}
