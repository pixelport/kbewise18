package de.htw.ai.kbe;

import de.htw.ai.kbe.storage.DependencyBinder;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/rest/*")
public class App extends ResourceConfig {

    public App() {
        packages("de.htw.ai.kbe.services");
        register(new DependencyBinder());
        // Enable Tracing support.
        property(ServerProperties.TRACING, "ALL");
    }
}
