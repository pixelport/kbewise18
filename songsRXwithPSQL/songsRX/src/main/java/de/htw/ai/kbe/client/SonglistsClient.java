package de.htw.ai.kbe.client;

import de.htw.ai.kbe.beans.Song;
import de.htw.ai.kbe.beans.Songlist;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;

public class SonglistsClient {

    private static Client client = ClientBuilder.newClient();
    private static WebTarget baseTarget = client
            .target("http://localhost:8080/songsRX/rest/songLists");

    private static String getToken(String username) {
        return client.target("http://localhost:8080/songsRX/rest/auth?userId=" + username).request(MediaType.TEXT_PLAIN).get(String.class);
    }

    private static void listAllSonglistsAsJSON() {
        System.out.println("----- Listing all songlist entries -> mmuster -----");
        System.out.println("Generating TOKEN requests ...");
        String mmuster_token = getToken("mmuster");
        String eschuler_token = getToken("eschuler");
        System.out.println("mmuster token " + mmuster_token);
        System.out.println("eschuler token " + eschuler_token);
        System.out.println("Generating GET request ...");
        String all_mmuster_songlists = baseTarget.queryParam("userId", "mmuster")
                .request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, mmuster_token).get(String.class);
        System.out.println("Printing all songlists that are held by mmuster ...");
        System.out.println(all_mmuster_songlists);
        System.out.println("Generating GET request ...");
        String public_mmuster_songlists = baseTarget.queryParam("userId", "mmuster")
                .request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, eschuler_token).get(String.class);
        System.out.println("Printing only the public songlists held by mmuster, because they were requested by eschuler");
        System.out.println(public_mmuster_songlists);
    }

    private static void listAllSonglistsAsXML() {
        System.out.println("----- Listing all songlist entries -> mmuster -----");
        System.out.println("Generating TOKEN requests ...");
        String mmuster_token = getToken("mmuster");
        String eschuler_token = getToken("eschuler");
        System.out.println("mmuster token " + mmuster_token);
        System.out.println("eschuler token " + eschuler_token);
        System.out.println("Generating GET request ...");
        String all_mmuster_songlists = baseTarget.queryParam("userId", "mmuster")
                .request(MediaType.APPLICATION_XML).header(HttpHeaders.AUTHORIZATION, mmuster_token).get(String.class);
        System.out.println("Printing all songlists that are held by mmuster ...");
        System.out.println(all_mmuster_songlists);
        System.out.println("Generating GET request ...");
        String public_mmuster_songlists = baseTarget.queryParam("userId", "mmuster")
                .request(MediaType.APPLICATION_XML).header(HttpHeaders.AUTHORIZATION, eschuler_token).get(String.class);
        System.out.println("Printing only the public songlists held by mmuster, because they were requested by eschuler");
        System.out.println(public_mmuster_songlists);
    }

    private static void listSpecificSonglistAsJSON(Integer id) {
        System.out.println("----- Listing specific songlist with id " + id + " -----");
        System.out.println("Generating TOKEN requests ...");
        String mmuster_token = getToken("mmuster");
        String eschuler_token = getToken("eschuler");
        System.out.println("mmuster token " + mmuster_token);
        System.out.println("eschuler token " + eschuler_token);
        System.out.println("Generating GET request ...");
        System.out.println("Generating GET request ...");
        String list = baseTarget.path(String.valueOf(id)).request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, mmuster_token).get(String.class);
        System.out.println(list);
        //String forbidden = baseTarget.path(String.valueOf(id)).request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, eschuler_token).get(String.class);
        //System.out.println(forbidden);
    }

    private static void listSpecificSonglistAsXML(Integer id) {
        System.out.println("----- Listing specific songlist with id " + id + " -----");
        System.out.println("Generating TOKEN requests ...");
        String mmuster_token = getToken("mmuster");
        String eschuler_token = getToken("eschuler");
        System.out.println("mmuster token " + mmuster_token);
        System.out.println("eschuler token " + eschuler_token);
        System.out.println("Generating GET request ...");
        System.out.println("Generating GET request ...");
        String list = baseTarget.path(String.valueOf(id)).request(MediaType.APPLICATION_XML).header(HttpHeaders.AUTHORIZATION, mmuster_token).get(String.class);
        System.out.println(list);
        //String forbidden = baseTarget.path(String.valueOf(id)).request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, eschuler_token).get(String.class);
        //System.out.println(forbidden);
    }

    private static void postSpecificSonglistAsJSON() {
        System.out.println("----- Saving specific songlist to the database -----");
        System.out.println("Generating TOKEN requests ...");
        String mmuster_token = getToken("mmuster");
        String eschuler_token = getToken("eschuler");
        System.out.println("mmuster token " + mmuster_token);
        System.out.println("eschuler token " + eschuler_token);
        Set<Song> songs = new HashSet<>();
        songs.add(new Song.Builder( "Cant Stop The Feeling", "Justin Timberlake").album("Trolls").released("2016-11-11").build());
        Songlist to_be_persisted = new Songlist(null, true, songs);
        Entity<Songlist> songlistEntity = Entity.json(to_be_persisted);
        Response response = baseTarget.request().accept(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, mmuster_token).post(songlistEntity);
        System.out.println("Response status: " + response.getStatus() + " URI " + response.getHeaderString("Location"));
    }

    private static void postSpecificSonglistAsXML() {
        System.out.println("----- Saving specific songlist to the database -----");
        System.out.println("Generating TOKEN requests ...");
        String mmuster_token = getToken("mmuster");
        String eschuler_token = getToken("eschuler");
        System.out.println("mmuster token " + mmuster_token);
        System.out.println("eschuler token " + eschuler_token);
        Set<Song> songs = new HashSet<>();
        songs.add(new Song.Builder( "Bad Things", "Camila Cabello, Machine Gun Kelly").
                album("Bloom").released("2018-05-17").build());
        Songlist to_be_persisted = new Songlist(null, true, songs);
        Entity<Songlist> songlistEntity = Entity.xml(to_be_persisted);
        Response response = baseTarget.request().accept(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, mmuster_token).post(songlistEntity);
        System.out.println("Response status: " + response.getStatus() + " URI " + response.getHeaderString("Location"));
    }


    private static void postInvalidSonglistAsJSON() {
        System.out.println("----- Saving invalid songlist to database (SHOULD PRODUCE Status 400) -----");
        String mmuster_token = getToken("mmuster");
        String eschuler_token = getToken("eschuler");
        System.out.println("mmuster token " + mmuster_token);
        System.out.println("eschuler token " + eschuler_token);
        Set<Song> songs = new HashSet<>();
        songs.add(new Song.Builder( "Bad Things", "Camila Cabello, Machine Gun Kelly").
                album("Bloom").released("2018-05-17").build());
        songs.add(new Song.Builder("Successfull", "Drake").album("So Far Gone").released("2012-03-10").build());
        Songlist to_be_persisted = new Songlist(null, false, songs);
        Entity<Songlist> songlistEntity = Entity.json(to_be_persisted);

        Response response = baseTarget.request().accept(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, mmuster_token).post(songlistEntity);
        System.out.println("Response status: (SHOULD BE 400) " + response.getStatus());
    }

    private static void deleteSpecificSonglist(Integer id) {
        System.out.println("----- Deleting specific songlist from the database -----");
        System.out.println("Generating TOKEN requests ...");
        String mmuster_token = getToken("mmuster");
        String eschuler_token = getToken("eschuler");
        System.out.println("mmuster token " + mmuster_token);
        System.out.println("eschuler token " + eschuler_token);
        Response response = baseTarget.path(String.valueOf(id)).request().header(HttpHeaders.AUTHORIZATION, mmuster_token).delete();
        System.out.println("Response status: " + response.getStatus());
    }

    public static void main(String[] args) {
        listAllSonglistsAsJSON();
        listAllSonglistsAsXML();
        listSpecificSonglistAsJSON(18);
        listSpecificSonglistAsXML(18);
        postSpecificSonglistAsXML();
        postInvalidSonglistAsJSON();
        //postSpecificSonglistAsJSON();
        //postSpecificSonglistAsXML();
    }
}
