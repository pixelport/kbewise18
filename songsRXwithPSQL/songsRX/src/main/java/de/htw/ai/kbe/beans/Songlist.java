package de.htw.ai.kbe.beans;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "songlist")
@XmlRootElement(name = "songlist")
public class Songlist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer list_id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner")
    private User owner;
    private boolean isPublic;
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinTable(name = "songlist_song",
            joinColumns = {@JoinColumn(name = "list_id")},
            inverseJoinColumns = {@JoinColumn(name = "song_id")})
    private Set<Song> songs;

    public Songlist() {

    }

    public Songlist(Integer list_id, User owner, boolean isPublic, Set<Song> songs) {
        this.list_id = list_id;
    }

    public Songlist(User owner, boolean isPublic, Set<Song> songs) {
        this.owner = owner;
        this.isPublic = isPublic;
        this.songs = songs;
    }

    public void setList_id(Integer id) {
        this.list_id = id;
    }

    public Integer getList_id() {
        return list_id;
    }

   public void setOwner(User owner) {
        this.owner = owner;
    }

    public User getOwner() {
        return owner;
    }

    public void setIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    public boolean getIsPublic() {
        return isPublic;
    }

    public Set<Song> getSongs() {
        return songs;
    }


    public void addSong(Song s) {
        if (songs == null) {
            songs = new HashSet<>();
        }
        songs.add(s);
    }

    public void setSongs(Set<Song> songs) {
        this.songs = songs;
    }
}
