package de.htw.ai.kbe.storage;

import de.htw.ai.kbe.storage.songlists.RelationalSonglistDB;
import de.htw.ai.kbe.storage.songlists.UserSonglists;
import de.htw.ai.kbe.storage.songs.InMemorySongDB;
import de.htw.ai.kbe.storage.songs.Playlist;
import de.htw.ai.kbe.storage.users.InMemoryUserDB;
import de.htw.ai.kbe.storage.users.UserGroup;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.inject.Singleton;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class DependencyBinder extends AbstractBinder {
    @Override
    protected void configure() {
        bind(Persistence.createEntityManagerFactory("songDB-PU"))
                .to(EntityManagerFactory.class);
        bind(Playlist.class).to(InMemorySongDB.class);
        bind(UserGroup.class).to(InMemoryUserDB.class).in(Singleton.class);
        bind(UserSonglists.class).to(RelationalSonglistDB.class);
    }
}
