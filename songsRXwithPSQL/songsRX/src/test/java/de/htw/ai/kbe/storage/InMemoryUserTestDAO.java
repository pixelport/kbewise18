package de.htw.ai.kbe.storage;

import de.htw.ai.kbe.beans.Song;
import de.htw.ai.kbe.beans.User;
import de.htw.ai.kbe.storage.songs.InMemorySongDB;
import de.htw.ai.kbe.storage.users.InMemoryUserDB;
import de.htw.ai.kbe.storage.users.UserGroup;
import org.apache.commons.lang3.RandomStringUtils;

import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryUserTestDAO extends UserGroup{

    public static String getValidTestUsername(){
        return "testUser";
    }

    public InMemoryUserTestDAO(){
        super(null);
    }

    @Override
    public List<User> loadUsers(){
        // don't load users from db
        // load test user database
        List<User> testUsers = new LinkedList<User>();
        testUsers.add(new User.Builder(getValidTestUsername()).build());
        return testUsers;
    }
}
