package de.htw.ai.kbe.api;

import de.htw.ai.kbe.services.SongsWebService;
import de.htw.ai.kbe.services.UsersWebService;
import de.htw.ai.kbe.storage.InMemoryUserTestDAO;
import de.htw.ai.kbe.storage.songs.InMemorySongDB;
import de.htw.ai.kbe.storage.users.InMemoryUserDB;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Singleton;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class AuthTest extends JerseyTest {

    @Override
    protected Application configure() {
        AbstractBinder binder = new AbstractBinder() {
            @Override
            protected void configure() {
                bind(de.htw.ai.kbe.storage.InMemoryUserTestDAO.class).to(InMemoryUserDB.class).in(Singleton.class);
            }
        };

        ResourceConfig config = new ResourceConfig(UsersWebService.class).register(binder);
        final ServiceLocator locator = ServiceLocatorUtilities.bind(binder);
        locator.inject(this);
        return config;
    }

    @Test
    public void authGenerateTokenForValidUsername(){
        Response response =  target("/auth").queryParam("userId", InMemoryUserTestDAO.getValidTestUsername())
                .request(MediaType.TEXT_PLAIN).buildGet().invoke();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(response.readEntity(String.class).length(), 30);
    }

    @Test
    public void authUnknownUserReturns403Status(){
        Response response =  target("/auth").queryParam("userId", "unknownUser")
                .request(MediaType.TEXT_PLAIN).buildGet().invoke();
        Assert.assertEquals(403, response.getStatus());
    }
}
