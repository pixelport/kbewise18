package de.htw.ai.kbe.storage;

import de.htw.ai.kbe.beans.Song;
import de.htw.ai.kbe.storage.songs.InMemorySongDB;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InMemorySongsDAO implements InMemorySongDB{
    private static Map<Integer, Song> storage;
    private static int autoIncrementId = 0;

    public InMemorySongsDAO(){
        storage = new ConcurrentHashMap<Integer, Song>();
    }

    @Override
    public List<Song> GETSongs() {
        return new ArrayList<Song>(storage.values());
    }

    @Override
    public Song GETSong(Integer id) {
        return storage.get(id);
    }

    @Override
    public boolean POSTSong(Song s) {
        if(s.getId() == null){
            s.setId(autoIncrementId);
            autoIncrementId++;
        }
        if(storage.get(s.getId()) != null){
            return false;
        }
        else {
            storage.put(s.getId(), s);
            return true;
        }
    }

    @Override
    public void PUTSong(Song payload, Song real_song) {
        payload.setId(real_song.getId());
        storage.put(real_song.getId(), payload);
    }

    @Override
    public boolean DELETESong(Integer id) {
        return false;
    }
}
