package de.htw.ai.kbe.api;

import de.htw.ai.kbe.beans.Song;
import de.htw.ai.kbe.services.SongsWebService;
import de.htw.ai.kbe.services.UsersWebService;
import de.htw.ai.kbe.storage.InMemorySongsDAO;
import de.htw.ai.kbe.storage.InMemoryUserTestDAO;
import de.htw.ai.kbe.storage.songs.InMemorySongDB;


import de.htw.ai.kbe.storage.songs.Playlist;
import de.htw.ai.kbe.storage.users.InMemoryUserDB;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;

public class SongsTest extends JerseyTest {

    @Inject
    private InMemorySongDB songsDB;

    private String validToken = null;

    @Override
    protected Application configure() {
        AbstractBinder binder = new AbstractBinder() {
            @Override
            protected void configure() {
                bind(de.htw.ai.kbe.storage.InMemorySongsDAO.class).to(InMemorySongDB.class).in(Singleton.class);
                bind(de.htw.ai.kbe.storage.InMemoryUserTestDAO.class).to(InMemoryUserDB.class).in(Singleton.class);
            }
        };

        ResourceConfig config = new ResourceConfig(SongsWebService.class, UsersWebService.class).register(binder);
        final ServiceLocator locator = ServiceLocatorUtilities.bind(binder);
        locator.inject(this);
        return config;
    }

    private String GenerateValidAuthorizationToken() {
        return target("/auth").queryParam("userId", InMemoryUserTestDAO.getValidTestUsername())
                .request(MediaType.TEXT_PLAIN).buildGet().invoke().readEntity(String.class);
    }

    @Before
    public void init() {
        // POST test songs and get valid token
        validToken = GenerateValidAuthorizationToken();
        Assert.assertEquals(validToken.length(), 30);
        Song song = generateTestSong();
        Entity<Song> songEntity = Entity.json(song);
        Response response = target("/songs")
                .request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, validToken).buildPost(songEntity)
                .invoke();
    }

    @Test
    public void PUTSongTest() {
        // modify song with a put request
        Song updatedSong = generateTestSong();
        updatedSong.setId(0);
        updatedSong.setTitle(updatedSong.getTitle() + " - modified");

        Entity<Song> songEntity = Entity.json(updatedSong);

        Response response = target("/songs/" + updatedSong.getId())
                .request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, validToken).buildPut(songEntity).invoke();
        Assert.assertEquals(204, response.getStatus());

        // Test if Song has been updated
        Assert.assertEquals(updatedSong.getTitle(), songsDB.GETSong(updatedSong.getId()).getTitle());
    }

    private Song generateTestSong(){
        Song song = new Song();
        song.setAlbum("testAlbum");
        song.setTitle("testTitle");
        song.setArtist("testArtist");
        song.setReleased(new Date(1544990329696L).toString());
        return song;
    }
}
